FROM node:14-alpine

WORKDIR /usr/src/app

COPY package*.json ./

RUN yarn 

COPY index.html svelte.config.cjs tsconfig.json vite.config.js ./
COPY ./src src
COPY ./public public

RUN yarn build

EXPOSE 5000

ENV HOST=0.0.0.0

CMD [ "yarn", "serve" ]
